import '../styles/users.css'
import React from 'react';
import { useEffect, useState } from 'react';
import axios from 'axios';

export default function User() {

    const [user, setUser] = useState({});

    function getUser() {
        axios.get("api/users/keagan")
            .then(res => {
                console.log(res.data)
                setUser(res.data);
            });
    }

    useEffect(() => getUser(), []);

    return (
        <div id="user-container">
            <header>User</header>
            <div>{`${user.username} ${user.email}`}</div>
        </div>
    );
}